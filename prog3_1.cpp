/*
    Compile with:
    g++ prog3_1.cpp -o prog3_1 -I lua-5.3.4/src -L lua-5.3.4/src -l lua -l m -l dl
*/

extern "C"{
    #include "lua.h"
    #include "lualib.h"
    #include "lauxlib.h"
}

int main(int argc, char *argv[]) {
    printf("Assignment #3-1, Thomas Webb, tjwebb@comcast.net\n");
    // validate command line input
    if (argc != 2) {
        printf("Error: must have 1 argument\n");
        return 0;
    }
    lua_State *L = luaL_newstate(); // create the new lua state
    luaL_openlibs(L);
    luaL_dofile(L,argv[1]);         // run the provided file
    lua_close(L);
    return 0;
}
